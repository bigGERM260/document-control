﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Document_Control.Objects;
using System.Windows.Forms;
using Controls = System.Windows.Controls;
using System.Text.RegularExpressions;
using System.Configuration;

/// <summary>
/// Author:     Germaine Robinson
/// </summary>


namespace Document_Control
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Main : Window
    {
        // ----------------------------------------------------
        // ------------------- Properties ---------------------

        // Variables
        System.Diagnostics.Process process = new System.Diagnostics.Process();
        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();

        public List<Objects.File> SelectedDrawings { get; set; }
        String basePath;// = "C:\\Program Files\\";
        String basePath_Alerts = "";
        String basePath_Paint = "";

        TreeViewItem expandedNode;

        // ------------------- Properties ---------------------
        // ----------------------------------------------------
        // ----------------- Initialization -------------------

        public Main()
        {
            InitializeComponent();

            try
            {
                //basePath = System.Configuration.ConfigurationSettings.AppSettings["InitialDirectory"];
                basePath = ConfigurationManager.AppSettings["InitialDirectory"];
                System.Windows.MessageBox.Show("Path: " + basePath, "Base Path",
                    MessageBoxButton.OK, MessageBoxImage.Information);
                if (String.IsNullOrEmpty(basePath))
                    basePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            }
            catch (Exception ex)
            {
                basePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }
            

            Populate_DocumentControl();
            tvDocument_Control.IsExpanded = true;
            tvDocument_Control.Header = basePath;
        }

        /// <summary>
        /// This populates the files & folders in the M:\Prints\_Alerts\ directory
        /// </summary>
        public void Populate_Alerts()
        {
            foreach (string dir in Directory.GetDirectories(basePath_Alerts))
            {
                TreeViewItem item = new TreeViewItem();
                item.Header = dir;
                item.Tag = dir;
                item.FontWeight = FontWeights.Normal;
                //item.Expanded += new RoutedEventHandler(Folder_Populate);
                item.Selected += new RoutedEventHandler(Folder_Populate);
                tvAlerts.Items.Add(item);
            }
        }

        /// <summary>
        /// This populates the files & folders in the M:\Prints\_Paint\ directory
        /// </summary>
        public void Populate_Paint()
        {
            foreach (string dir in Directory.GetDirectories(basePath_Paint))
            {
                TreeViewItem item = new TreeViewItem();
                item.Header = dir;
                item.Tag = dir;
                item.FontWeight = FontWeights.Normal;
                //item.Expanded += new RoutedEventHandler(Folder_Populate);
                item.Selected += new RoutedEventHandler(Folder_Populate);
                tvPaint.Items.Add(item);
            }
        }

        /// <summary>
        /// This populates the files & folders in the M:\Document Control\ directory
        /// </summary>
        public void Populate_DocumentControl()
        {
            foreach (string dir in Directory.GetDirectories(basePath))
            {
                Folder folder = new Folder(dir);

                TreeViewItem item = new TreeViewItem();
                item.Header = folder.folder;
                item.Tag = folder.directoryPath;
                item.FontWeight = FontWeights.Normal;
                //item.Expanded += new RoutedEventHandler(Folder_Populate);
                item.Selected += new RoutedEventHandler(Folder_Populate);
                tvDocument_Control.Items.Add(item);
            }
        }

        // ----------------- Initialization -------------------
        // ----------------------------------------------------
        // -------------------- Methods -----------------------

        /// <summary>
        /// This populates the subfolders when the folder is expanded
        /// </summary>
        /// <param name="item"></param>
        public void PopulateFolder(ref TreeViewItem item)
        {
            // clear out files
            listView.Items.Clear();

            if (item.Items.Count <= 1) { // this is needed to prevent recursive event call (child -> parent -> grandparent...)

                item.Items.Clear();
                //listView.Items.Clear();
                try
                {
                    foreach (string dir in Directory.GetDirectories(item.Tag.ToString()))
                    {
                        Folder folder = new Folder(dir);

                        TreeViewItem subitem = new TreeViewItem();
                        subitem.Header = folder.folder;
                        subitem.Tag = folder.directoryPath;
                        subitem.FontWeight = FontWeights.Normal;
                        //subitem.Expanded += new RoutedEventHandler(Folder_Populate);
                        subitem.Selected += new RoutedEventHandler(Folder_Populate);
                        item.Items.Add(subitem);
                    }

                    // reset listView.view to default
                    listView.View = null;

                    foreach (string filePath in Directory.GetFiles(item.Tag.ToString()))
                    {
                        Objects.File file = new Objects.File(filePath);

                        Controls.ListViewItem subitem = new Controls.ListViewItem();
                        subitem.Content = file.fileName;
                        subitem.Tag = file.filePath;
                        subitem.FontWeight = FontWeights.Normal;
                        subitem.MouseDoubleClick += new MouseButtonEventHandler(File_Selected);
                        listView.Items.Add(subitem);
                    }
                }
                catch (Exception ex)
                {
                    ShowErrorMessage("Unable to expand the folder.", ex);
                }
            }
        }

        /// <summary>
        /// This opens the selected file from the listview
        /// </summary>
        /// <param name="item"></param>
        public void OpenFile(String filePath)
        {
            try
            {
                // run the selected file
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.EnableRaisingEvents = false;
                process.StartInfo.FileName = filePath;
                process.Start();
            }
            catch (Exception ex)
            {
                ShowErrorMessage(String.Format("Unable to open {0}.", "Failure to Launch!"), ex);
            }
        }

        /// <summary>
        /// This searches the specified directory for matching files
        /// </summary>
        /// <param name="strPattern"></param>
        public void FileSearch(String strPattern)
        {
            try
            {
                // 3/31/2020 - GR - built-in getFiles throws an error when the user doesn't have permissions to all sub-directories of M:\Document Control\
                //var matchingFiles = Directory.GetFiles(basePath, strPattern, SearchOption.AllDirectories).ToList(); 
                //var matchingFiles = RecursiveGetMatchingFiles(basePath, strPattern);
                var regExPattern = WildCardToRegularExpression(strPattern);
                var matchingFiles = RecursiveGetMatchingFiles(basePath, regExPattern);

                // set columns and bindings needed to view file & path
                listView.Items.Clear();
                GridView myGridView = new GridView();
                myGridView.AllowsColumnReorder = true;
                GridViewColumn gvc1 = new GridViewColumn();
                gvc1.DisplayMemberBinding = new System.Windows.Data.Binding("fileName");
                gvc1.Header = "File Name";
                gvc1.Width = 300;
                myGridView.Columns.Add(gvc1);
                GridViewColumn gvc2 = new GridViewColumn();
                gvc2.DisplayMemberBinding = new System.Windows.Data.Binding("filePath");
                gvc2.Header = "Path";
                gvc2.Width = 600;
                myGridView.Columns.Add(gvc2);
                listView.View = myGridView;

                // add matching files to ListView
                foreach (var mFile in matchingFiles)
                {
                    Objects.File file = new Objects.File(mFile);

                    Controls.ListViewItem subitem = new Controls.ListViewItem();
                    subitem.Content = file; // String.Format("{0}\t\tDirectory: {1}", file.fileName, file.filePath);
                    subitem.Tag = file.filePath;
                    subitem.FontWeight = FontWeights.Normal;
                    //subitem.Selected += new RoutedEventHandler(File_Selected);
                    subitem.MouseDoubleClick += new MouseButtonEventHandler(File_Selected);
                    listView.Items.Add(subitem);
                }
            } catch (Exception ex)
            {
                ShowErrorMessage("Unable to search for matching files.", ex);
            }
        }

        /// <summary>
        /// This recursively searches the sub-directories of a particular parent directory
        /// for files matching the specified string pattern.
        /// </summary>
        /// <param name="currentDirectory"></param>
        /// <param name="strPattern"></param>
        /// <returns></returns>
        public List<String> RecursiveGetMatchingFiles(String currentDirectory, String strPattern)
        {
            List<String> matchingFiles = new List<String>();

            // add matching files in current directory
            foreach (String file in Directory.GetFiles(currentDirectory))
            {
                var fileName = System.IO.Path.GetFileName(file);

                //if (file.ToLower().Contains(strPattern.ToLower()))
                if (Regex.IsMatch(fileName.ToLower(), strPattern.ToLower()))
                {
                    matchingFiles.Add(file);
                }
                    
            }

            foreach (String directory in Directory.GetDirectories(currentDirectory))
            {
                try
                {
                    // merge matching files from recursive call to sub-directory
                    var recurMatches = RecursiveGetMatchingFiles(directory, strPattern);
                    if (recurMatches.Count > 0)
                        matchingFiles = matchingFiles.Concat(recurMatches).ToList();
                }
                catch (UnauthorizedAccessException auex)
                {
                    // ignore access denied, continue to next directory (iteration of loop)
                }
                catch (Exception ex)
                {
                    ShowErrorMessage(String.Format("Unable to recursively retrieve files matching the String Pattern: {0} Errored out in Directory: {1}.", strPattern, currentDirectory), ex);
                }
            }

            return matchingFiles;
        }

        /// <summary>
        /// This converts the user friendly shorthand wildcard character (*)
        /// to its string literal regular expression counter part
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public String WildCardToRegularExpression(String file)
        {
            return "^" + Regex.Escape(file).Replace("\\*", ".*") + "$";
        }

        /// <summary>
        /// This shows an error message
        /// </summary>
        /// <param name="errorMsg"></param>
        /// <param name="ex"></param>
        public void ShowErrorMessage(String errorMsg, Exception ex)
        {
            System.Windows.MessageBox.Show(String.Format("{0}\n\nException: {1}", errorMsg, ex.Message), "Error!",
                MessageBoxButton.OK, MessageBoxImage.Error);
        }

        // -------------------- Methods -----------------------
        // ----------------------------------------------------
        // --------------------- Events -----------------------

        /// <summary>
        /// This populates the expanded folder with the sub-folders and files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Folder_Populate(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)sender;
            PopulateFolder(ref item);
            e.Handled = true; // this is needed to prevent recursive event call (child -> parent -> grandparent...)
        }

        /// <summary>
        /// This handles the event when a item from the listview is selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void File_Selected(object sender, RoutedEventArgs e)
        {
            Controls.ListViewItem item = (Controls.ListViewItem)sender;
            OpenFile(item.Tag.ToString());
        }

        void DoNothing(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// This searches for files matching the pattern
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            FileSearch(txtSearch.Text);
        }

        // --------------------- Events -----------------------
        // ----------------------------------------------------

    }
}
