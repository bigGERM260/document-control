﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Document_Control.Objects
{
    public class Folder
    {
        // ----------------------------------------------------
        // ------------------- Properties ---------------------

        public String folder { get; set; }
        public String directoryPath { get; set; }

        // ------------------- Properties ---------------------
        // ----------------------------------------------------

        public Folder(String directoryPath)
        {
            this.directoryPath = directoryPath;
            
            if (directoryPath.Length > 0 && directoryPath.Contains("\\"))
            {
                //int folderIndex = directoryPath.LastIndexOf("\\");
                //this.folder = directoryPath.Substring(folderIndex, directoryPath.Length - folderIndex);
                this.folder = directoryPath.Split('\\').ToList().Last();    // gets the last element of the list
            }
            else
            {
                this.folder = directoryPath;
            }
        }

    }
}
