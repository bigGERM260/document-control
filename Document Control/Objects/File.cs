﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Document_Control.Objects
{
    public class File
    {
        // ----------------------------------------------------
        // ------------------- Properties ---------------------

        public String filePath { get; set; }
        public String fileName { get; set; }

        // ------------------- Properties ---------------------
        // ----------------------------------------------------

        public File(String filePath)
        {
            this.filePath = filePath;
            this.fileName = filePath.Split('\\').ToList().Last();
        }

    }
}
